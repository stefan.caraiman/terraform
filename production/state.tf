terraform {

  backend "s3" {
    bucket         = "terraform-production-workshop"
    encrypt        = true
    key            = "terraform-dana-production.tfstate"
    region         = "eu-north-1"
    dynamodb_table = "lock-tf"
  }
}

